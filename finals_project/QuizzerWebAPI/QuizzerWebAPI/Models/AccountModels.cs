﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzerWebAPI.Models
{
    public class AccountModels
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public int  StudentNumber{ get; set; }

        public string Course { get; set; }

        public string Section { get; set; }

        public string Year { get; set; }

    }
}
