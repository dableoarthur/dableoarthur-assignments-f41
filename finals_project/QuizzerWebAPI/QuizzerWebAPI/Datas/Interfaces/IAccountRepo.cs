﻿using QuizzerWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzerWebAPI.Datas
{
    public interface IAccountRepo
    {
        List<AccountModels> GetAllAccounts();
        AccountModels GetAccountById(int Id);

        void InsertAccount(AccountModels account);

        void UpdateAccount(AccountModels account);

        void DeleteAccount(AccountModels account);

        void Save();
    }
}
