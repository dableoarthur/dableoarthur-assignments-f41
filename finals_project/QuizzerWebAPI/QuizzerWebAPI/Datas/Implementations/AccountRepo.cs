﻿using QuizzerWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizzerWebAPI.Datas.Implementations
{
    public class AccountRepo : IAccountRepo
    {
        private readonly ApplicationDbContext _context;

        public AccountRepo(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }

        public void DeleteAccount(AccountModels account)
        {
            _context.Accounts.Remove(account);
        }

        public AccountModels GetAccountById(int Id)
        {
            return _context.Accounts.Where(x => x.Id == Id).FirstOrDefault();
        }

        public List<AccountModels> GetAllAccounts()
        {
            return _context.Accounts.ToList();
        }

        public void InsertAccount(AccountModels account)
        {
            _context.Accounts.Add(account);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void UpdateAccount(AccountModels account)
        {
            _context.Accounts.Update(account);
        }
    }
}
