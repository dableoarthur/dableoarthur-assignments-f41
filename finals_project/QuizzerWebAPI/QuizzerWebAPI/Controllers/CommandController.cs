﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using QuizzerWebAPI.Datas;
using QuizzerWebAPI.Datas.Implementations;
using QuizzerWebAPI.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuizzerWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommandController : ControllerBase
    {

        private readonly IAccountRepo _account;
        private IMapper _mapper;

        public CommandController(IAccountRepo account, IMapper mapper)
        {
            _account = account;
            _mapper = mapper;
             
        }
        // GET: api/<CommandController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            var AllAccounts = _account.GetAllAccounts();
                      
            return Ok(AllAccounts);
        }

        // GET api/<CommandController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            var accountItem = _accountRepo.GetAccountById(id);

            return ;
        }
         
        // POST api/<CommandController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<CommandController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<CommandController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
