

namespace assignment_8.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class account
    {
        public int id { get; set; }
        [Required(ErrorMessage ="This Field is Required")]
        public string username { get; set; }
        [Required(ErrorMessage = "This Field is Required")]
        [DataType(DataType.Password)]
        public string password { get; set; }
        [Required(ErrorMessage = "This Field is Required")]
        [DataType(DataType.Password)]
        [DisplayName ("Confirm Password") ]
        [Compare("password")]
        public string confirm { get; set; }
        [Required(ErrorMessage = "This Field is Required")]
        public string firstname { get; set; }
        [Required(ErrorMessage = "This Field is Required")]
        public string middlename { get; set; }
        [Required(ErrorMessage = "This Field is Required")]
        public string lastname { get; set; }
        [Required(ErrorMessage = "This Field is Required")]
        public int age { get; set; }
        [Required(ErrorMessage = "This Field is Required")]
        public string address { get; set; }
        [Required(ErrorMessage = "This Field is Required")]
        public string sex { get; set; }
    }
}
