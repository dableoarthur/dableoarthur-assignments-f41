#pragma checksum "C:\Users\Jason\Desktop\calculator\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "be0a74c61fa73c656e1b4fc02c0f9d3107f4fbdf"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Jason\Desktop\calculator\Views\_ViewImports.cshtml"
using calculator;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Jason\Desktop\calculator\Views\_ViewImports.cshtml"
using calculator.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"be0a74c61fa73c656e1b4fc02c0f9d3107f4fbdf", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1a03cd1fb36737993ae185cb05589563696a4502", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\Jason\Desktop\calculator\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div id=\"calculator\"  style=\"width: 25%; margin-left: 35%;\" >\r\n\t\t<div >\r\n\t    <span v-for=\"log in logs\">{{ log }}</span>\r\n\t  </div>\r\n\r\n\t   <input type=\"string\" class=\"form-control mb-3\"  v-model=\"value\" ");
            WriteLiteral("@keyup.enter=\"getResult()\">\r\n\r\n\r\n\t   <div >\r\n\t\t    <div >\r\n\t\t      <button class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(7)\">7</button>\r\n\t\t   \r\n\t\t      <button  class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(8)\">8</button>\r\n\t\t   \r\n\t\t      <button   class=\"btn btn-success mb-2\"");
            WriteLiteral("@click=\"addExpresion(9)\">9</button>\r\n\r\n\t\t      <button  class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"clear()\">C</button>\r\n\r\n\t\t      <button  class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"clearE()\">CE</button>\r\n\r\n\t\t      \r\n\t\t    </div>\r\n  \t\t</div>\r\n\r\n  \t\t<div>\r\n\t\t    <div >\r\n\t\t      <button  class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(4)\">4</button>\r\n\t\t \r\n\t\t      <button  class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(5)\">5</button>\r\n\t\t  \r\n\t\t      <button  class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(6)\">6</button>\r\n\t\t  \r\n\t\t  \t<button class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(\'/\')\">/</button>\r\n\r\n\t\t  \t<button   class=\"btn btn-success mb-2\"");
            WriteLiteral("@click=\"addExpresion(\'*\')\">*</button>\r\n\r\n\t\t      \r\n\t\t    </div>\r\n\t  </div>\r\n\r\n\t  <div >\r\n\t\t    <div >\r\n\t\t      <button class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(1)\">1</button>\r\n\t\t  \r\n\t\t      <button class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(2)\">2</button>\r\n\t\t    \r\n\t\t      <button class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(3)\">3</button>\r\n\t\t      <button  class=\"btn btn-success mb-2\" style=\"width: 70px\"");
            WriteLiteral("@click=\"addExpresion(\'-\')\">-</button>\r\n\t\t    \r\n\t\t      \r\n\t\t</div>\r\n  \t</div>\r\n \t\r\n  \t<div >\r\n\t    <div>\r\n\t      <button class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(0)\">0</button>\r\n\t    \r\n\t      <button class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"addExpresion(\'.\')\">.</button>\r\n\t    \r\n\t      <button class=\"btn btn-success mb-2\" ");
            WriteLiteral("@click=\"getResult()\">=</button>\r\n\r\n\t      <button class=\"btn btn-success mb-2\" style=\"width: 70px\" ");
            WriteLiteral("@click=\"addExpresion(\'+\')\">+</button>\r\n\t    </div>\r\n  </div>\r\n\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
