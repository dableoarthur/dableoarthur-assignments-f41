﻿using BlotterRecords.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlotterRecords.Service.Interfaces
{
   public interface IBlotter
    {
        List<BlotterModel> GetAllBlotters();
        BlotterModel GetBlotterById(int Id);

        void InsertBlotter(BlotterModel blotter);

        void UpdateBlotter(BlotterModel blotter);

        void DeleteBlotter(BlotterModel blotter);

        void Save();
    }
}
