﻿using BlotterRecords.DataAccess.Model;
using BlotterRecords.Service.Data;
using BlotterRecords.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlotterRecords.Service.Implementation
{
    public class BlotterRepo : IBlotter
    {
        private readonly AppDbContext _context;

        public BlotterRepo(AppDbContext appDbContext)
        {
            _context = appDbContext;
        }

        public void DeleteBlotter(BlotterModel blotter)
        {
            _context.Blotters.Remove(blotter);
        }

        public List<BlotterModel> GetAllBlotters()
        {
            return _context.Blotters.ToList();
        }

        public BlotterModel GetBlotterById(int Id)
        {
            return _context.Blotters.Where(x =>x.Id==Id).FirstOrDefault();
            
        }

        public void InsertBlotter(BlotterModel blotter)
        {
            _context.Blotters.Add(blotter);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void UpdateBlotter(BlotterModel blotter)
        {
            _context.Blotters.Update(blotter);
        }
    }
}
