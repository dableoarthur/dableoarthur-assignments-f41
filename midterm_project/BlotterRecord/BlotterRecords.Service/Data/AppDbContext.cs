﻿using BlotterRecords.DataAccess.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;


namespace BlotterRecords.Service.Data
{
   public class AppDbContext : IdentityDbContext
    {

        public virtual DbSet<BlotterModel> Blotters { get; set; }
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                  .UseMySQL("server=localhost;port=3306;user=root;password=;database=BlotterDB")
                  .UseLoggerFactory(LoggerFactory.Create(b => b
                          .AddConsole()
                          .AddFilter(level => level >= LogLevel.Information)))
                          .EnableSensitiveDataLogging()
                          .EnableDetailedErrors();


        }
    }
}
