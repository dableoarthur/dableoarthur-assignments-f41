﻿using AutoMapper;
using BlotterRecords.DataAccess.Model;
using BlotterRecords.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlotterRecords.Web.Helper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<BlotterModel, BlotterViewModel>();
            CreateMap<BlotterModel, EditBlotterViewModel>();
            CreateMap<BlotterModel, DetailsBlotterViewModel>();
            CreateMap<BlotterModel, DeleteBlotterViewModel>().ReverseMap();
            CreateMap<CreateBlotterViewModel, BlotterModel >();
            CreateMap<BlotterModel, PostBlotterViewModel>();
            CreateMap<PostBlotterViewModel, BlotterModel>();
            CreateMap<EditPostViewModel, BlotterModel>();

        }
      

    }
}
