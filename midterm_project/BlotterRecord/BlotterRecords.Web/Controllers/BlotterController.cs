﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlotterRecords.DataAccess.Model;
using BlotterRecords.Service.Interfaces;
using BlotterRecords.Web.Helper;
using BlotterRecords.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace BlotterRecords.Web.Controllers
{
    
    public class BlotterController : Controller
    {
        private readonly IBlotter _blotter;
        private IMapper _mapper;
        private readonly IWebHostEnvironment webHostEnvironment;

        public BlotterController(IBlotter blotter, IMapper mapper, IWebHostEnvironment webHost)
        {
            _blotter = blotter;
            _mapper = mapper;
            webHostEnvironment = webHost;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index()
        {
            var AllBlotters = _blotter.GetAllBlotters();
            var mappedBlotters = _mapper.Map<List<BlotterViewModel>>(AllBlotters);
           
            return View(mappedBlotters);
        }

        [HttpGet]
      
        public IActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Delete(int Id)
        {
            var blotter = _blotter.GetBlotterById(Id);
            var mappedBlotter = _mapper.Map<DeleteBlotterViewModel>(blotter);
            return View(mappedBlotter);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var blotter = _blotter.GetBlotterById(id);

            EditBlotterViewModel editBlotterViewModel = new EditBlotterViewModel
            {
                Id = blotter.Id,
                Name = blotter.Name,
                Complain = blotter.Complain,
                Date = blotter.Date,
                Officer = blotter.Officer,
                ExistingPicture = blotter.Picture
            };
            
            return View(editBlotterViewModel);
        }

        [HttpGet]
        public IActionResult Details(int Id)
        {
            var blotter = _blotter.GetBlotterById(Id);
            var mappedBlotter = _mapper.Map<DetailsBlotterViewModel>(blotter);
            return View(mappedBlotter);
        }


        [HttpPost]
        public IActionResult Delete(DeleteBlotterViewModel viewModel)
        {
            
            var mappedBlotter = _mapper.Map<BlotterModel>(viewModel);
            _blotter.DeleteBlotter(mappedBlotter);
            _blotter.Save();

            return RedirectToAction("Index", "Blotter");
        }

        [HttpPost]
        public IActionResult Edit(EditBlotterViewModel viewModel)
        {
            FileUpload fileUpload = new FileUpload(webHostEnvironment);
            var blotterCurrent = _blotter.GetBlotterById(viewModel.Id);
            blotterCurrent.Name = viewModel.Name;
            blotterCurrent.Complain = viewModel.Complain;
            blotterCurrent.Date = viewModel.Date;
            blotterCurrent.Officer = viewModel.Officer;
            if (viewModel.Picture != null) 
            {
                string ImageFile = fileUpload.UploadFile(viewModel.Picture);
                blotterCurrent.Picture = ImageFile;
            }                      
           
             var mappedBlotter = _mapper.Map<BlotterModel>(blotterCurrent);
            _blotter.UpdateBlotter(mappedBlotter);
            _blotter.Save();

            return RedirectToAction("Index", "Blotter");
        }

        [HttpPost]
        public IActionResult Create(CreateBlotterViewModel viewModel)
        {
            FileUpload fileUpload = new FileUpload(webHostEnvironment);
            string ImageFile = fileUpload.UploadFile(viewModel.Picture);
            var blotter = new PostBlotterViewModel
            {
                Name = viewModel.Name,
                Complain = viewModel.Complain,
                Date = viewModel.Date,
                Picture = ImageFile,
                Officer = viewModel.Officer
                 
             };
            var mappedBlotter = _mapper.Map<BlotterModel>(blotter);
            _blotter.InsertBlotter(mappedBlotter); 
            _blotter.Save();

            return RedirectToAction("Index", "Blotter");
        }

    }
}
