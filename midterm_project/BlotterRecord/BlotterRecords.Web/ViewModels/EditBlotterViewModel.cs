﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlotterRecords.Web.ViewModels
{
    public class EditBlotterViewModel : CreateBlotterViewModel
    {

        public int Id { get; set; }

        public string ExistingPicture { get; set; }
    }
}
