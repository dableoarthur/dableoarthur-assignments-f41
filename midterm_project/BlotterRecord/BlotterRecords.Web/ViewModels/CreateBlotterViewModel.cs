﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlotterRecords.Web.ViewModels
{
    public class CreateBlotterViewModel
    {

        public string Name { get; set; }

        public string Complain { get; set; }

        public DateTime Date { get; set; }

        public IFormFile Picture { get; set; }

        public string Officer { get; set; }
    }
}
