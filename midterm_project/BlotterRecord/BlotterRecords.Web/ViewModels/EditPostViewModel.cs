﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlotterRecords.Web.ViewModels
{
    public class EditPostViewModel
    {
        public string Name { get; set; }

        public string Complain { get; set; }

        public DateTime Date { get; set; }

        public string Picture { get; set; }

        public string Officer { get; set; }
    }
}
