﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlotterRecords.Web.ViewModels
{
    public class BlotterViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Complain { get; set; }

        public DateTime Date { get; set; }

    }
}
