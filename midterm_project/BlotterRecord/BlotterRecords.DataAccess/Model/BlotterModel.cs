﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace BlotterRecords.DataAccess.Model
{
   public class BlotterModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Complain { get; set; }

        public DateTime Date { get; set; }

        public string Picture { get; set; }

        public string Officer { get; set; }
    }
}
