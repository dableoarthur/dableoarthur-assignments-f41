﻿using System;
using System.Collections.Generic;  
using System.Linq;  
using System.Text;  
using System.Threading.Tasks; 

namespace Calculate
{
    //Class for Basic Calculation
    public class basicCalculator {

              
            // Add two integers and returns the sum  
            public int Add(int num1, int num2){  
                return num1 + num2;  
            }  
        
            // Multiply two integers and retuns the result  
            public int Multiply(int num1, int num2){  
                return num1 * num2;  
            }  
            // Subtracts small number from big number  
            public int Subtract(int num1, int num2){  
                    if (num1 > num2){  
                        return num1 - num2;  
                    }  
        
                    return num2 - num1;  
        
            }  
            //performing Division on two float variables.  
            public float Division(float num1, float num2){  
                return num1 / num2;  
            }  
    }

    //Class for Coversion of Decimal to Binary
    public class Conversion{

            //Conversion from Decimal to Binary
            public string DecimalToBinary(int num){
                 
                int arr;   
                string answer ="";
                for(int i=0; num>0; i++)      
                    {      
                    arr=num%2;      
                    num= num/2;    
                    answer += arr.ToString();
                    }      
                    
                             
                return answer;
            }
    } 

    //Class for Scientific Calculation
    public class Scientific{

        //Factorial of a number
        public int Factorial(int num)
        {
            int i = 1;
            for (int s = 1; s <= num; s++){
               
                i = i * s;
            }
            return i;
        }
        //Square of number
        public int Squared(int num){

            return num * num;

        }

        //Cube of number
        public int Cube(int num){

            return num * num * num;
        }
        //Square Root
        public double SquareRoot(double num){

            return Math.Sqrt(num);
        }
        //Power of Power
        public double RaisePower(int bas, int pow){

            return Math.Pow(bas, pow);
        }




    }  
        
    

}
