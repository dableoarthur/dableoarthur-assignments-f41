using System;

namespace MVCForm.Models
{
    public class Employee
    {
     
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MidName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        
        public int Age { get; set; }
        public string pretext { get; set; }

    }
}

